
const image = document.querySelector(".image-to-show");
const arr = ["./img/1.jpg", "./img/2.jpg", "./img/3.JPG", "./img/4.png"];
const btn = document.querySelector("button");

let activeImageIndex = 0;
let changeImgInterval = null;

function changeImg() {
    if(arr.length-1 === activeImageIndex){
        activeImageIndex = 0;
    }else(
        activeImageIndex++
    )
    const activeImageSRC = arr[activeImageIndex];
    image.src = activeImageSRC;
}

function start (){
    if(changeImgInterval){
        clearInterval(changeImgInterval);
        changeImgInterval = null;
        btn.style.backgroundColor = "";
        btn.innerHTML = "Натисніть, щоб почати"
    
    }else{
        changeImgInterval = setInterval(changeImg, 3000);
        btn.innerHTML = "Натисніть, щоб зупинити!"
        btn.style.backgroundColor = "aqua";
    }    
}


btn.addEventListener("click", start);